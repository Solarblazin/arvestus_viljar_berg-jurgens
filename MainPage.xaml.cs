﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Arvestus_Viljar_Berg_Jürgens
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private int loginKatse = 0;

        public MainPage()
        {
            this.InitializeComponent();
        }
        private void Key_Press_Password(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter)
            {
                this.Button_Click(null, null);
            }
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string kasutajanimi = "user";
            string parool = "SecretPassword";

            if (loginKatse < 3)
            {
                if (textbox1.Text == kasutajanimi)
                {
                    if (passwordbox1.Password == parool)
                    { 
                        this.Frame.Navigate(typeof(Content));
                    }
                    else
                    {
                        textblock1.Text = "Logimine ebaõnnestus!";
                        loginKatse++;
                    }
                }
                else
                {
                    textblock1.Text = "Logimine ebaõnnestus!";
                    loginKatse++;
                }
            }
            else
            {
                textblock1.Text = "Sisselogimise katsed on otsas!";
            }
        }
    }
}
